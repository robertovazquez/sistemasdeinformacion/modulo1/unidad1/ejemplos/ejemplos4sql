﻿DROP DATABASE IF EXISTS urbanizacion1;
CREATE DATABASE urbanizacion1;
USE urbanizacion1;


CREATE OR REPLACE TABLE zonaurbana(
nombrezona varchar (20),
  categoria varchar (10),
  PRIMARY KEY (nombrezona)


);

CREATE OR REPLACE TABLE bloquecasa (
calle varchar (20),
 numero int,
 npisos int,
  zona varchar (20),
  PRIMARY KEY (calle,numero),
  
  CONSTRAINT fkbloquezona FOREIGN KEY (zona)
  REFERENCES zonaurbana (nombrezona) 

);


CREATE OR REPLACE TABLE casaparticular (
   zona varchar (20),
   numero int,
   metros2 varchar (20),
  PRIMARY KEY (zona,numero),
  
CONSTRAINT fkcasazona FOREIGN KEY (zona)
  REFERENCES zonaurbana (nombrezona)


);


CREATE OR REPLACE TABLE piso(
 calle varchar (20),
  numero int,
  planta varchar (10),
  puerta varchar (10),
  metros2 varchar (20),
  PRIMARY KEY (calle,numero,planta,puerta),
CONSTRAINT fkpisobloque FOREIGN KEY (calle,numero)
  REFERENCES bloquecasa (calle,numero)

);

CREATE OR REPLACE TABLE persona (
dni int,
  nombre varchar (20),
  edad date,
  vivecasazona varchar (20),
  vivezonanumero int,
  vivepisocalle varchar (20),
  vivepisonumero int,
  vivepisoplanta varchar (20),
  vivepisopuerta varchar (20),
  PRIMARY KEY (dni),
CONSTRAINT fkpersonacasa FOREIGN KEY (vivecasazona,vivezonanumero)
  REFERENCES casaparticular (zona,numero),

CONSTRAINT fkpersonaspiso FOREIGN KEY (vivepisocalle,vivepisonumero,vivepisoplanta,vivepisopuerta)
  REFERENCES piso (calle,numero,planta,puerta)

);

CREATE OR REPLACE TABLE poseecasa(
   dnipersona int,
   zonacasa varchar (20),
   numerocasa int,
   PRIMARY KEY (dnipersona,zonacasa,numerocasa),
CONSTRAINT fkposeepersona FOREIGN KEY (dnipersona)
  REFERENCES persona (dni),
CONSTRAINT fkposeeparticular FOREIGN KEY (zonacasa,numerocasa)
  REFERENCES casaparticular (zona,numero)


);


CREATE OR REPLACE TABLE poseepiso(
   dnipersona int,
   pisocalle varchar(20),
   pisonumero int,
  pisopiso varchar(10),
  pisopuerta varchar(10),
   PRIMARY KEY (dnipersona,pisocalle,pisonumero,pisopiso,pisopuerta),
CONSTRAINT fkposeepisopersona FOREIGN KEY (dnipersona)
  REFERENCES persona (dni),
CONSTRAINT fkposeepiso FOREIGN KEY (pisocalle,pisonumero,pisopiso,pisopuerta)
  REFERENCES piso (calle,numero,planta,puerta)


);




